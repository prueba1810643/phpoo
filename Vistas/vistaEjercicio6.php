<?php
// Hecho por Argueta Bravo Angel Jacob
// Incluye las clases Carro, Avion, Barco y Anfibio
include_once('../Clases/ejercicio6/Carro4.php');
include_once('../Clases/ejercicio6/Avion.php');
include_once('../Clases/ejercicio6/Barco.php');
include_once('../Clases/ejercicio6/Anfibio.php');

// Verifica si el formulario ha sido enviado (POST)
if (!empty($_POST)){
    // Declaración de un operador switch para determinar el tipo de transporte seleccionado
    switch ($_POST['tipo_transporte']) {
        case 'aereo':
            // Creación del objeto avión con sus respectivos parámetros para el constructor
            $jet1 = new Avion('jet', '400', 'gasoleo', '2');
            // Llama al método resumenAvion y guarda el resultado en la variable $mensaje
            $mensaje = $jet1->resumenAvion();
            break;
        case 'terrestre':
            // Creación del objeto carro con sus respectivos parámetros para el constructor
            $carro1 = new Carro('carro', '200', 'gasolina', '4');
            // Llama al método resumenCarro y guarda el resultado en la variable $mensaje
            $mensaje = $carro1->resumenCarro();
            break;
        case 'maritimo':
            // Creación del objeto barco con sus respectivos parámetros para el constructor
            $bergantin1 = new Barco('bergantin', '40', 'na', '15');
            // Llama al método resumenBarco y guarda el resultado en la variable $mensaje
            $mensaje = $bergantin1->resumenBarco();
            break;
        case 'anfibio':
            // Creación del objeto anfibio con sus respectivos parámetros para el constructor
            $anfibio1 = new Anfibio('Hydra Spyder', '120', 'hibrido gasolina y electricidad', 'híbrido', 'chorros de agua');
            // Llama al método resumenAnfibio y guarda el resultado en la variable $mensaje
            $mensaje = $anfibio1->resumenAnfibio();
            break;
    }
}
?>
<!DOCTYPE html>
<html>
<head>
    <!-- Inclusión de archivos de estilo y scripts de Bootstrap y jQuery -->
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/bootstrap-grid.css">
    <script type="text/javascript" src="../js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../js/jquery-3.4.1.min.js"></script>
    <title>Indice</title>
</head>
<body>
    <!-- Contenedor principal -->
    <div class="container" style="margin-top: 4em">
        <!-- Encabezado -->
        <header>
            <h1>Los transportes</h1>
        </header><br>
        <!-- Formulario de selección de tipo de transporte -->
        <form method="post">
            <div class="form-group">
                <label for="CajaTexto1">Tipo de transporte:</label>
                <!-- Menú desplegable con opciones de transporte -->
                <select class="form-control" name="tipo_transporte" id="CajaTexto1">
                    <option value='aereo' >Aereo</option>
                    <option value='terrestre' >Terrestre</option>
                    <option value='maritimo' >Maritimo</option>
                    <option value='anfibio' >Anfibio</option>
                </select>
            </div>
            <!-- Botones para enviar el formulario y regresar -->
            <button class="btn btn-primary" type="submit" >enviar</button>
            <a class="btn btn-link offset-md-8 offset-lg-9 offset-6" href="../index.php">Regresar</a>
        </form>
    </div>
    <!-- Contenedor para mostrar la respuesta del servidor -->
    <div class="container mt-5">
        <h1>Respuesta del servidor</h1>
        <!-- Tabla para mostrar el resumen del transporte seleccionado -->
        <table class="table">
            <thead>
                <tr>
                    <th>Transporte</th>
                </tr>
            </thead>
            <tbody>
                <!-- Muestra el contenido de la variable $mensaje, que contiene el resumen del transporte -->
                <?= $mensaje; ?>
            </tbody>
        </table>
    </div>
</body>
</html>
