<?php
// Hecho por Argueta Bravo Angel Jacob 
// Incluye la clase Carro2
include_once('../Clases/ejercicio2/Carro2.php');

?>

<!DOCTYPE html>
<html>
<head>
    <!-- Incluye archivos de estilo y scripts de Bootstrap y jQuery -->
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/bootstrap-grid.css">
    <script type="text/javascript" src="../js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../js/jquery-3.4.1.min.js"></script>
    <title>Ejercicio 2</title>
</head>
<body>
    <!-- Contenedor principal -->
    <div class="container" style="margin-top: 4em">
        <!-- Encabezado -->
        <header><h1>La verificación</h1></header><br>
        <!-- Formulario para ingresar datos del carro -->
        <form method="post">
            <!-- Grupo de campos para color, modelo y año de fabricación -->
            <div class="form-group row">
                <label class="col-sm-1" for="CajaTexto1">Color:</label>
                <div class="col-sm-4">
                    <input class="form-control" type="color" name="color" id="CajaTexto1" required>
                </div>

                <label class="col-sm-2 pl-md-5" for="CajaTexto2">Modelo:</label>
                <div class="col-sm-4 pl-lg-1">
                    <input class="form-control" type="text" name="modelo" id="CajaTexto2" required>
                </div>

                <!-- Input para el año de fabricación -->
                <label class="col-sm-2 pl-lg-1" for="AnoFabricacion">Año de fabricación:</label>
                <div class="col-sm-4 pl-lg-1">
                    <input class="form-control" type="month" name="ano_fabricacion" id="AnoFabricacion" required>
                </div>
            </div>
            <!-- Botones para enviar el formulario y regresar -->
            <button class="btn btn-primary" type="submit">Enviar</button>
            <a class="btn btn-link offset-md-9 offset-lg-9 offset-7" href="../index.php">Regresar</a>
        </form>
    </div>

    <!-- Contenedor para mostrar la respuesta del servidor -->
    <div class="container mt-5">
        <!-- Encabezado -->
        <h1>Respuesta del servidor</h1>
        <!-- Tabla para mostrar las características del carro y el resultado de la verificación -->
        <table class="table">
            <thead>
                <tr>
                    <th>Características de carro</th>
                    <th>Resultado de verificación</th>
                </tr>
            </thead>
            <tbody>
                <!-- Fila para mostrar el color del carro -->
                <tr>
                    <td><?= 'Color:' ?></td>
                    <td><?= $Carro1->color ?></td>
                </tr>
                <!-- Fila para mostrar el modelo del carro -->
                <tr>
                    <td><?= 'Modelo:' ?></td>
                    <td><?= $Carro1->modelo ?></td>
                </tr>
                <!-- Fila para mostrar el resultado de la verificación -->
                <tr>
                    <td><?= 'Verificación:' ?></td>
                    <td><?= $Carro1->getVerificacionResult() ?></td>
                </tr>
            </tbody>
        </table>
    </div>
</body>
</html>
