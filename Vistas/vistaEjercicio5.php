<?php
// Hecho por Argueta Bravo Angel Jacob 
// Incluye la clase TokenAutodestructible
include_once('../clases/ejercicio5/TokenAutodestructible.php');

$mensaje = '';

// Verifica si el formulario ha sido enviado (POST)
if (!empty($_POST)) {
    // Obtiene el nombre desde el formulario
    $nombre = $_POST['nombre'];
    // Crea un objeto de la clase TokenAutodestructible
    $tokenAutodestructible = new TokenAutodestructible();
    // Genera un mensaje con el nombre y la contraseña generada por el objeto
    $mensaje = "Hola {$nombre}, la contraseña generada es: " . $tokenAutodestructible->mostrarPassword();
}

?>

<!DOCTYPE html>
<html>
<head>
    <!-- Incluye archivos de estilo y scripts de Bootstrap y jQuery -->
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/bootstrap-grid.css">
    <script type="text/javascript" src="../js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../js/jquery-3.4.1.min.js"></script>
    <title>Recoge tu token</title>
</head>
<body>
    <!-- Input de solo lectura para mostrar el mensaje generado -->
    <input class='form-control' type='text' value='<?= $mensaje ?>' readonly>

    <!-- Contenedor principal -->
    <div class="container" style="margin-top: 4em">
        <!-- Encabezado -->
        <header><h1>Recoge tu token</h1></header><br>
        <!-- Formulario para recoger el nombre -->
        <form method="post">
            <!-- Campo de entrada para el nombre -->
            <div class="form-group">
                <label for="CajaTexto1">Escribe tu nombre:</label>
                <input class="form-control" type="text" name="nombre" id="CajaTexto1">
            </div>
            <!-- Botones para enviar el formulario y regresar -->
            <button class="btn btn-primary" type="submit">Enviar</button>
            <a class="btn btn-link offset-md-8 offset-lg-9 offset-6" href="../index.php">Regresar</a>
        </form>
    </div>
</body>
</html>


