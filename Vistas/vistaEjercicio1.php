<?php
// Hecho por Argueta Bravo Angel Jacob
// Incluye las clases Carro y Moto
include_once('../Clases/ejercicio1/Carro.php');
include_once('../Clases/ejercicio1/Moto.php');
?>

<!DOCTYPE html>
<html>

<head>
    <!-- Inclusión de archivos de estilo y scripts de Bootstrap y jQuery -->
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/bootstrap-grid.css">
    <script type="text/javascript" src="../js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../js/jquery-3.4.1.min.js"></script>
    <title>Indice</title>
</head>

<body>
    <!-- Campo de entrada de solo lectura para mostrar el mensaje del servidor -->
    <input type="text" class="form-control" value="<?php echo $mensajeServidor; ?>" readonly>

    <!-- Div para insertar el mensaje del servidor para Moto -->

    <div class="container" style="margin-top: 4em">

        <!-- Encabezado y formulario para Carro y Moto -->
        <header>
            <h1>Carro y Moto</h1>
        </header><br>
        <form method="post">
            <!-- Campo de entrada para el color del carro -->
            <div class="form-group row">
                <label class="col-sm-3" for="CajaTexto1">Color del carro:</label>
                <div class="col-sm-4">
                    <input class="form-control" type="color" name="color" id="CajaTexto1">
                </div>
                <div class="col-sm-4"></div>
            </div>

            <!-- Nuevos campos de entrada para la clase Moto -->
            <div class="form-group row">
                <label class="col-sm-3" for="TipoMoto">Tipo de moto:</label>
                <div class="col-sm-4">
                    <input class="form-control" type="text" name="tipo" id="TipoMoto">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3" for="MarcaMoto">Marca de moto:</label>
                <div class="col-sm-4">
                    <input class="form-control" type="text" name="marca" id="MarcaMoto">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3" for="PotenciaMoto">Potencia de moto (HP):</label>
                <div class="col-sm-4">
                    <input class="form-control" type="text" name="potencia" id="PotenciaMoto">
                </div>
            </div>

            <!-- Botones de envío y regreso -->
            <button class="btn btn-primary" type="submit">Enviar</button>
            <a class="btn btn-link offset-md-8 offset-lg-9 offset-6" href="../index.php">Regresar</a>
        </form>

    </div>

</body>

</html>
