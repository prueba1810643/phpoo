<?php
//Hecho por Argueta Bravo Angel Jacob
	class Moto {
		// Declaración de propiedades
		public $tipo;
		public $marca; 
		public $potencia; 
	}

	// Inicializamos el mensaje que lanzará el servidor con vacío
	$mensajeServidor = '';

	// Creamos la instancia un objeto de la clase Moto
	$Moto1 = new Moto;

	if (!empty($_POST)) {
		// Almacenamos los valores enviados por POST en los atributos de la instancia Moto
		$Moto1->tipo = $_POST['tipo'];
		$Moto1->marca = $_POST['marca']; 
		$Moto1->potencia = $_POST['potencia']; 
		
		// Construimos el mensaje que será lanzado por el servidor
		$mensajeServidor = 'El servidor dice que ya escogiste una moto ' . $_POST['tipo'] . ' de la marca ' . $_POST['marca'] . ' con una potencia ' . $_POST['potencia'] . ' HP';
	}
?>
