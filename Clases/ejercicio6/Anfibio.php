<?php
//Hecho por Argueta Bravo Angel Jacob
// Incluye el archivo transporte.php que contiene la clase Transporte
include_once('transporte.php');

// Definición de la clase Anfibio que hereda de la clase Transporte
class Anfibio extends Transporte {
    // Propiedades privadas específicas para Anfibio
    private $tipo_motor;
    private $propulsion_agua;

    // Constructor de la clase Anfibio
    public function __construct($nom, $vel, $com, $motor, $propulsion_agua) {
        // Llama al constructor de la clase padre (Transporte)
        parent::__construct($nom, $vel, $com);

        // Inicializa las propiedades específicas de Anfibio
        $this->tipo_motor = $motor;
        $this->propulsion_agua = $propulsion_agua;
    }

    // Método para obtener un resumen específico de Anfibio
    public function resumenAnfibio() {
        // Llama al método crear_Ficha de la clase padre y concatena información específica de Anfibio
        $mensaje = parent::crear_Ficha();
        $mensaje .= '<tr>
                        <td>Tipo de motor:</td>
                        <td>' . $this->tipo_motor . '</td>
                    </tr>
                    <tr>
                        <td>Propulsión en agua:</td>
                        <td>' . $this->propulsion_agua . '</td>
                    </tr>';
        return $mensaje;
    }
}

?>
