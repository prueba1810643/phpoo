<?php
//Hecho por Argueta Bravo Angel Jacob
// Definición de la clase TokenAutodestructible
class TokenAutodestructible
{
    // Propiedad privada para almacenar la contraseña
    private $password;

    // Constructor que se ejecuta al crear una instancia de la clase
    public function __construct()
    {
        // Llama al método para generar la contraseña
        $this->generarPassword();
    }

    // Método  para generar una contraseña aleatoria
    private function generarPassword()
    {
        // Llama al método para generar una cadena aleatoria y asigna el resultado a la propiedad password
        $this->password = $this->generarCadenaAleatoria(4);
    }

    // Método  para generar una cadena aleatoria de la longitud especificada
    private function generarCadenaAleatoria($longitud)
    {
        // Caracteres  para la cadena aleatoria
        $caracteres = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $cadenaAleatoria = '';

        // Ciclo para generar la cadena aleatoria
        for ($i = 0; $i < $longitud; $i++) {
            $cadenaAleatoria .= $caracteres[rand(0, strlen($caracteres) - 1)];
        }

        // Retorna la cadena aleatoria generada
        return $cadenaAleatoria;
    }

    // Método público para mostrar la contraseña
    public function mostrarPassword()
    {
        return $this->password;
    }

    // Método destructor que se ejecuta cuando se destruye la instancia de la clase
    public function __destruct()
    {
        // Imprime un mensaje mostrando la contraseña
        echo "Contraseña: {$this->password}";
    }
}

?>
