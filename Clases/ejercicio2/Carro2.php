<?php
//Hecho por Argueta Bravo Angel Jacob
// Definición de la clase Carro2
class Carro2
{
    // Propiedades públicas
    public $color;
    public $modelo;

    // Propiedad privada para el resultado de la verificación
    private $verificacionResult;

    // Método para realizar la verificación según el año de fabricación
    public function verificacion($anoFabricacion)
    {
        // Verifica el año de fabricación y asignar el resultado
        if ($anoFabricacion < 1990) {
            $this->verificacionResult = 'No';
        } elseif ($anoFabricacion >= 1990 && $anoFabricacion <= 2010) {
            $this->verificacionResult = 'Revisión';
        } elseif ($anoFabricacion > 2010) {
            $this->verificacionResult = 'Sí';
        }
    }

    // Método para obtener el resultado de la verificación
    public function getVerificacionResult()
    {
        return $this->verificacionResult;
    }
}

// Creación de una instancia de la clase Carro2
$Carro1 = new Carro2();

// Verificación si se ha enviado el formulario ($_POST no está vacío)
if (!empty($_POST)) {
    // Asignar valores a las propiedades de la instancia según los datos del formulario
    $Carro1->color = $_POST['color'];
    $Carro1->modelo = $_POST['modelo'];

    // Obtener el año de fabricación desde el nuevo input del formulario
    $anoFabricacion = $_POST['ano_fabricacion'];

    // Realizar la verificación
    $Carro1->verificacion($anoFabricacion);
}

?>

