# Control de Versiones con Git
PHP Orientado a objetos
## Descripción
El lenguaje de programación PHP incorpora algunos conceptos del paradigma orientado a objetos tales como la herencia,
 el encapsulamiento y también es posible incorporar variantes del polimorfismo. Esto ha permitido la creación de librerías
  y marcos de desarrollo para facilitar la creación de sitios y sistemas Web. En el presente curso se abordan los conceptos 
  elementales del paradigma orientado a objetos utilizando PHP.
## Autor

* Argueta Bravo Angel Jacob 
### Contacto
angeljacob938@gmail.com

## Instructor
* Daniel Barajas González.
### Contacto
ldanielbg@comunidad.unam.mx